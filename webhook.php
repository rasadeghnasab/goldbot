<?php
require_once 'includes/initial.php';

$webhook_actions = [
	'set',
    'delete'
];
if(isset($_GET['webhook'])
   && !empty($_GET['webhook'])
   && in_array( $_GET['webhook'],  $webhook_actions))
{
	if($_GET['webhook'] == 'set') {
		// set webhook
		var_dump( setWebhook_method(BOT_WEBHOOK_URL) );
		echo 'webhook wat set to:' . BOT_WEBHOOK_URL;
	}
	else {
		// delete webhook
		var_dump( deleteWebhook_method() );
	}
}