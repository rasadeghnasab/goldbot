<?php
// Define the core paths
// Define them as absolute paths to make sure that require_once works as expected
// DIRECTORY_SEPARATOR is a PHP pre-defined constant
// (\ for Windows, / for Unix)
defined( 'DS' ) ? NULL : define( 'DS', DIRECTORY_SEPARATOR );
$Document_root = preg_replace( '/[\\\\\/]/i', DS, $_SERVER['DOCUMENT_ROOT'] );
defined( 'BOT_FOLDER' ) ? NULL : define( 'BOT_FOLDER', 'bota6s54dA654sadX654C6544F' );
if ( isset( $_SERVER['COMPUTERNAME'] ) ) {
	defined( 'SITE_ROOT' ) ? NULL : define( 'SITE_ROOT', 'F:\xampp\htdocs\goldbot' );
} else {
	defined( 'SITE_ROOT' ) ? NULL : define( 'SITE_ROOT', $Document_root . DS . BOT_FOLDER );
}
defined( 'LIB_PATH' ) ? NULL : define( 'LIB_PATH', SITE_ROOT . DS . 'includes' );
defined( 'CLASS_PATH' ) ? NULL : define( 'CLASS_PATH', SITE_ROOT . DS . 'includes' . DS . 'classes' . DS );

require_once( LIB_PATH . DS . 'configs.php' );
require_once( SITE_ROOT . DS . 'vendor/autoload.php' );
// load basic functions next so that everything after can use them
if ( ! isset( $_SERVER['COMPUTERNAME'] ) ) {
	require_once( LIB_PATH . DS . 'db_connection.php' );
	require_once( LIB_PATH . DS . 'bot_functions.php' );
	//require_once( LIB_PATH . DS . 'functions.php' );
	require_once( SITE_ROOT . DS . 'bot/categories.php' );
	//	require_once( SITE_ROOT . DS . 'bot/user/keyboards.php' );
	require_once( SITE_ROOT . DS . 'bot/admin/admins_auth.php' );

	// it must loads last
	require_once( LIB_PATH . DS . 'global_function_calls.php' );
	//	require_once( SITE_ROOT . DS . 'bot/admin/admin_loader.php' );

	//	/*
	if ( $global_user_type == 'admin' ) {
		require_once( SITE_ROOT . DS . 'bot/admin/admin_loader.php' );
	}
	else {
		require_once( SITE_ROOT . DS . 'bot/user/user_loader.php' );
	}
	//*/
	//	require_once( SITE_ROOT . DS . 'bot/view_changer.php' );
	require_once( SITE_ROOT . DS . 'includes/maintenance_mode.php' );
	require_once( SITE_ROOT . DS . 'bot/start.php' );
	require_once( LIB_PATH . DS . 'callback_executors.php' );
}