<?php
if($enableMaintenanceMode) {
	$allowedUsers = [
		126506089,
		266851390,
		111833936,
		394421136,
	];

	if(!in_array( $global_user->id, $allowedUsers )) {
		global $global_user;
		$return = "\n";
		$message = "🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧";
		$message .= $return;
		$message .= "متاسفانه بات در حالت تعمیر و نگهداری می باشد";
		$message .= $return;
		$message .= "لطفا برای مطلع شدن از زمان بازگشایی بات";
		$message .= $return;
		$message .= "به @rosadeghi پیام بدهید";
		;
		sendMessage_method($message);

		exit;
	}
}