<?php

$telegram_api_start_url = 'https://api.telegram.org/bot' . BOT_TOKEN . '/';

/********************************************
 *
 * Get message
 *
 *******************************************/
function get_json() {
	return file_get_contents( 'php://input' );
}

/**
 * get user full message
 */
function get_update() {
	$json = get_json();
	
	return json_decode( $json );
}

/**
 * get user sent text message
 */
function get_message_object() {
	global $global_update;

	$message_object = has_callback_query() ? $global_update->callback_query->message : $global_update->message;
	
	return $message_object;
}

/**
 * get user sent text message
 */
function get_message() {
	global $global_update;

	$message = has_callback_query() ? json_decode( $global_update->callback_query->data ) : persian_to_latin_numbers( $global_update->message->text );

	//	$message = $global_update->message->text;

	return $message;
}

/**
 * get user who sent the message as object
 *
 * @return string
 */
function get_user() {
	global $global_update;
	$user = has_callback_query() ? $global_update->callback_query->from : $global_update->message->from;

	return $user;
}

function user_find_or_create( $user = NULL ) {
	if ( ! $user ) {
		global $global_user;
		$user = $global_user;
	}
	
	try {
		//	$user = User::firstOrCreate( [
		$user = User::updateOrCreate( [ 'id' => $user->id ], [
			'id'         => isset( $user->id ) ? $user->id : $user->id,
			'username'   => isset( $user->username ) ? $user->username : NULL,
			'first_name' => isset( $user->first_name ) ? $user->first_name : NULL,
			'last_name'  => isset( $user->last_name ) ? $user->last_name : NULL,
		] );
	} catch( Exception $e ) {
		sd( $e );
	}

	return $user;
}

/**
 * check user type is "admin" or normal "user"
 *
 * @param null $user
 * @param bool $check_view If this parameter set to null, get user type from his view on database.
 *
 * @return string
 */
function get_user_type( $user = NULL, $check_view = TRUE ) {
	$user = $user ? $user : get_user();
	//	global $global_user_view;

	//	if ( $check_view ) {
	//		return $global_user_view;
	//	}
	
	if ( array_key_exists( $user->id, all_admins() ) ) {
		return 'admin';
	}
	
	return 'user';
}

function user_view_update( $view, $user = NULL ) {
	$user            = $user ?: get_user();
	$userModel       = User::find( $user->id );
	$userModel->view = $view;
	$userModel->save();
	
	return $userModel;
}

function get_user_view( $user = NULL ) {
	$user      = $user ? $user : get_user();
	$userModel = User::find( $user->id );
	
	return isset( $userModel->view ) ? $userModel->view : 'user';
}

/**
 * Send to see if user sends any object what we want or not? (for example: text, photo, sound and ...)
 *
 * @param string $object_type
 *
 * @return bool
 */
function check_user_send( $object_type = 'text' ) {
	global $global_message_object;
	
	switch ( $object_type ) {
		// text
		case 'text':
			$is_valid = isset( $global_message_object->text );
			break;
		// photo
		case 'photo':
			$is_valid = isset( $global_message_object->photo );
			break;
		// document
		case 'document':
			$is_valid = isset( $global_message_object->document );
			break;
		default:
			$is_valid = FALSE;
	}
	
	return $is_valid;
}

/**
 * Check to see if a user information in user table is complete or not?
 *
 * @param null $user
 *
 * @return bool
 */
function check_user_information( $user = NULL ) {
	$user = $user ? $user : get_user();
	
	$complete_user = TRUE;
	
	return (bool) $complete_user;
}

function all_admins() {
	global $admins_auth;
	
	return $admins_auth;
}

/**
 * get a special part of received message
 *
 * @param $part_name
 *
 * @return mixed
 */
function get_update_part( $part_name ) {
	global $global_update;
	$part = $global_update;
	
	return $part;
}

/**
 * Create parameters for url from an associative array
 *
 * @param      $query_data
 * @param null $numeric_prefix
 * @param null $arg_separator
 *
 * @return string
 */
function url_parameters_creator_from_array( $query_data ) {
	return http_build_query( $query_data );
}

/**
 * Complete a method and call that method from telegram.
 *
 * @param $method_name
 * @param $parameters
 *
 * @return mixed
 */
function method_usage( $method_name, $parameters = NULL ) {
	global $telegram_api_start_url;
	global $global_user;
	global $stop_method_execute;
	
	if ( ! check_method_exists( $method_name ) ) {
		method_not_exists_error( $method_name );
	}
	
	// check_method_exists();
	$url = $telegram_api_start_url . $method_name;
	
	if ( $parameters ) {
		$parameters['chat_id'] = isset( $parameters['chat_id'] ) ? $parameters['chat_id'] : $global_user->id;
		
		$url .= '?' . url_parameters_creator_from_array( $parameters );
	}
	
	if ( $stop_method_execute ) {
		return $url;
	}
	
	return method_fire( $url );
}

/**
 * Call url to trigger our method
 *
 * @param $url
 *
 * @return mixed
 */
function method_fire( $url ) {
	$response = json_decode( file_get_contents( $url ) );
	
	return $response;
}

function get_all_methods_names() {
	return [
		'sendMessage',
		'sendPhoto',
		'setWebhook',
		'sendChatAction',
	    'answerCallbackQuery',
	];
}

/*
 * Check a method exists or not
 *
 * @return bool
 */
function check_method_exists( $method ) {
	return in_array( $method, get_all_methods_names() );
}

/**
 * If a method does not exist, this method tell this error to user
 *
 * @param $method_name
 *
 * @return mixed
 */
function method_not_exists_error( $method_name ) {
	sendMessage_method( "Method '{$method_name}' does not exists" );
	exit;
}

/**
 * setWebhook method call
 *
 * @param $url
 *
 * @return mixed
 *
 */
function setWebhook_method( $url ) {
	$parameters = [
		'url' => $url,
	];
	
	return method_usage( 'setWebhook', $parameters );
}

function deleteWebhook_method() {
	return method_usage( 'setWebhook' );
}

function sendMessage_method( $text, $reply_markup = NULL, $chat_id = NULL, $reply_to_message_id = NULL, $parse_mode = 'html', $disable_web_page_preview = FALSE, $disable_notification = FALSE ) {
	$parameters = compact( 'text', 'reply_markup', 'chat_id', 'reply_to_message_id', 'parse_mode', 'disable_web_page_preview', 'disable_notification' );
	
	return method_usage( 'sendMessage', $parameters );
}

function sendPhoto_method( $photo, $caption = NULL, $reply_markup = NULL, $chat_id = NULL, $reply_to_message_id = NULL, $disable_notification = FALSE ) {
	$parameters = compact( 'photo', 'caption', 'reply_markup', 'chat_id', 'reply_to_message_id', 'disable_notification' );
	
	return method_usage( 'sendPhoto', $parameters );
}

function sendChatAction_method( $action = 'typing', $chat_id = NULL ) {
	return method_usage( 'sendChatAction', compact( 'action', 'chat_id' ) );
}

/**
 * @param array $options options can contain this keys ['text', 'show_alert', 'cache_time', 'url']
 *
 * @return mixed
 */
function answerCallbackQuery_method( $options = []) {
	global $global_update;
	$options['callback_query_id'] = $global_update->callback_query->id;

//	$parameters = compact( 'callback_query_id',  );

	return method_usage( 'answerCallbackQuery', $options );
}

function find_chat_action_in_url( $url ) {
	$pattern = '/(?<=send)(\w*)/';
	preg_match( $pattern, $url, $matches );
	
	$chatActionsArray = [
		'typing'            => 'Message',
		'upload_photo'      => 'Photo',
		'upload_video'      => 'Video',
		'upload_audio'      => 'Audio',
		'upload_document'   => 'Document',
		'find_location'     => 'Location',
		'upload_video_note' => 'VideoNote',
	];
	
	return array_search( $matches[0], $chatActionsArray ) ?: 'typing';
}

function message_queue( $messages_object ) {
	// we can only have one situation -> 1 keyboard, multiple messages (it can be photo, file, text or ...)
	return '';
}

function fetch_keyboard( $keyboard_name = 'main', $user_type = NULL ) {
	return json_encode( fetch_keyboard_array( $keyboard_name, $user_type ) );
}

function fetch_keyboard_array( $keyboard_name = 'main', $user_type = NULL ) {
	$user_type = $user_type ?: get_user_type();
	
	if ( $user_type == 'admin' ) {
		global $admin_keyboards;
		$keyboards = $admin_keyboards;
	} else {
		global $user_keyboards;
		$keyboards = $user_keyboards;
	}
	
	if ( isset( $keyboards[ $keyboard_name ] ) ) {
		$keyboard = $keyboards[ $keyboard_name ];
	} else {
		$keyboard = $keyboards['main'];
	}
	
	return $keyboard ?: NULL;
}

/**
 * @param       $message_function
 * @param array $parameters
 *
 * @return mixed|string
 */
function function_executor( $message_function, $parameters = array() ) {
	// execute message function and return message
	
	if ( function_exists( $message_function ) && ( is_array( $parameters ) || is_null( $parameters ) ) && is_callable( $message_function ) ) {
		$result = call_user_func_array( $message_function, $parameters );
	} else {
		$result = "function {{$message_function}} is not exists or parameters is not valid array";
	}
	
	return $result;
}

function message_to_states( $message = NULL ) {
	if ( ! $message ) {
		global $global_message;
		$message = $global_message;
	}
	
	global $message_to_state;
	
	$state = array_search( $message, $message_to_state );
	
	return $state ?: NULL;
}

function message_to_category( $message = NULL ) {
	global $categories_buttons;

	if ( ! $message ) {
		global $global_message;
		$message = $global_message;
	}

	$category_id = array_search( $message, $categories_buttons );

	return $category_id ?: NULL;
}

function get_user_state( $user = NULL ) {
	if ( ! $user ) {
		global $global_user;
		$user = $global_user;
	}
	
	$user_state = UserState::where( 'user_id', $user->id )->first();
	
	return $user_state;
}

function get_user_product( $user_state = NULL ) {
	if ( ! $user_state ) {
		global $global_user_state;
		$user_state = $global_user_state;
	}
	
	$user_product = isset( $user_state->product_id ) ? $user_state->product_id : NULL;
	
	return $user_product;
}

// insert product sections
function product_image_insert( $update, $product_id ) {
	// check if message contains photo
	if ( isset( $update->message->photo[0] ) ) {
		try {
			return ProductImage::updateOrCreate( // where clause
				[
					'product_id' => $product_id,
					'small'      => $update->message->photo[0]->file_id,
				], // properties
				[
					'medium' => $update->message->photo[1]->file_id,
					'large'  => $update->message->photo[2]->file_id,
				] );
		} catch( Exception $e ) {
			return FALSE;
		}
	}
	
	return FALSE;
}

//---------------- State handler

function get_states() {
	global $states;
	
	return $states;
}

function get_state( $state_name, $states = NULL ) {
	$states = $states ?: get_states();

	return (object) $states[ $state_name ];
}

/**
 * Handel states where you come in or out.
 *
 * @param      $state
 * @param bool $is_last
 *
 * @return object
 */
function state_handler( $state, $is_last = FALSE ) {
	// last state
	$new_state_name = last_state_handler( $state );
	$new_state      = next_state_handler( $new_state_name, $state );
	$result         = save_current_state( $new_state_name );

	// execute message and keyboard functions and return keyboard and message for this state
	return $new_state;
}

/**
 * execute the state but it's the latest state that user was in.
 *
 * @param $state
 *
 * @return mixed|string
 */
function last_state_handler( $state ) {
	////////////////////// check all, state properties
	// reply_validator
	if ( ! skip_this_state_execution( $state ) && state_is_not_valid( $state ) ) {
		return $state->name;
	}
	// enter callback (not needed)
	// exit callback (here we only need this function)
	if ( isset( $state->exit_callback ) && ! empty( $state->exit_callback ) && ! skip_this_state_execution( $state ) ) {
		// TODO:: there is no parameters, we must get parameters from somewhere (maybe user message)
		function_executor( $state->exit_callback );
	} elseif ( skip_this_state_execution( $state ) && strpos( $state->exit_callback, 'success_message' ) ) {
		function_executor( $state->exit_callback );
	}
	// next (not needed)
	$new_state = found_next_state_name( $state, get_states() );
	// prev (not needed)
	// keyboard (not needed)
	// message_function (not needed)
	//////////////////////////////////////////////////
	return $new_state;
}

/**
 * execute some of the current state functionality
 *
 * @param      $state
 * @param null $last_state
 *
 * @return object
 */
function next_state_handler( $state, $last_state = NULL ) {
	global $stop_method_execute;
	global $skip_state_execution;
	
	$keyboard = [ ];
	$message  = NULL;
	
	if ( ! is_object( $state ) ) {
		$state = get_state( $state );
	}

	///////// check all, state properties
	// enter callback (needed)
	if ( isset( $state->enter_callback ) && ! empty( $state->enter_callback ) ) {
		// TODO:: there is no parameters, we must get parameters from somewhere (maybe user message)
		function_executor( $state->enter_callback );
	}
	// exit callback (not needed)
	// next (not needed)
	// prev (not needed)
	// keyboard (needed)
	if ( isset( $state->keyboard ) && ! empty( $state->keyboard ) ) {
		$keyboard = fetch_keyboard( $state->keyboard );
	}

	if ( isset( $state->skippable ) && $state->skippable ) {
		$keyboard                 = json_decode( $keyboard, TRUE );
		$keyboard                 = isset( $keyboard['keyboard'] ) ? $keyboard : $keyboard['keyboard'] = [ ];
		$keyboard['keyboard'][][] = [ 'text' => $skip_state_execution ];
		
		$keyboard = json_encode( $keyboard );
	}
	
	// message_function (needed)
	if ( isset( $state->message_function ) && ! empty( $state->message_function ) ) {
		$message = function_executor( $state->message_function );
	}

	// If this state and last state are same, that means user send invalid message to us.
	if ( $state == $last_state && isset( $state->reply_validator ) && $state->next != $state->name ) {
		if ( is_array( $message ) ) {
			$stop_method_execute = TRUE;
			array_unshift( $message, function_executor( $state->reply_validator ) );
			$stop_method_execute = FALSE;
		} else {
			$message = function_executor( $state->reply_validator ) . $message;
		}
	}
	//////////////////////////////////////////////////
	$result = (object) [
		'keyboard' => $keyboard,
		'message'  => $message,
	];
	
	return $result;
}

function state_keyboard_existence_checker( $state ) {
	return isset( $state->keyboard ) ? $state->keyboard : NULL;
}

/**
 * @param string $state state name
 * @param null   $product_id
 * @param null   $user_id
 *
 * @return mixed
 */
function save_current_state( $state, $product_id = NULL, $user_id = NULL ) {
	if ( ! $user_id ) {
		global $global_user;
		$user_id = $global_user->id;
	}
	
	$model = $product_id ? compact( 'state', 'product_id' ) : compact( 'state' );
	
	$result = UserState::updateOrCreate( [ 'user_id' => $user_id ], $model );
	
	return $result;
}

/**
 * @param $state object
 *
 * @return string
 */
function found_next_state_name( $state ) {
	//	$new_state_name = ! isset( $state->next ) || empty( $state->next ) || $state->name == $state->next ?  : $state->next;

	if ( ! isset( $state->next ) || empty( $state->next ) || $state->name == $state->next ) {
		$new_state_name = message_to_states();
		if ( $new_state_name == NULL ) {
			$new_state_name = $state->next;
		}
	} else {
		$new_state_name = $state->next;
	}
	
	return $new_state_name;
}

function skip_this_state_execution( $state, $message = NULL ) {
	global $skip_state_execution;
	if ( ! $message ) {
		global $global_message;
		$message = $global_message;
	}
	
	return $message == $skip_state_execution && ( isset( $state->skippable ) && $state->skippable );
}

//*********************************** Wishlist
function toggleWishlist($user_id, $product_id) {
	$wishList = Wishlist::where('user_id', $user_id)->where('product_id', $product_id)->first();
	$result = [
		'action' => 'add',
	    'message' => 'رویدادی رخ نداد'
	];
	if($wishList) {
		$wishList->delete();
		$result['message'] = 'محصول مورد نظر از لیست علاقه مندی های شما حذف گردید';
		$result['action'] = 'delete';
	}
	else {
		Wishlist::create([
			'user_id' => $user_id,
			'product_id' => $product_id
		]);
		$result['message'] = 'محصول مورد نظر به لیست علاقه مندی های شما اضافه شد';
	}

	return $result;
}

//*********************************** Orders
function submitOrder($user_id, $product_id) {

}
//*********************************** Products
function admin_product_view( $products ) {
	global $stop_method_execute;
	$send_urls = [ ];
	
	$stop_method_execute = TRUE;
	foreach ( $products as $index => $product ) {
		$title = $product->title ? "💝💎{$product->title}💎💝\n\n" : NULL;
		$message  = '';
		$message .= $title;
		$message .= $product->weight ? "⚖️ وزن: " . number_format( $product->weight ) . " گرم\n" : NULL;
		$message .= $product->unit_price ? "💵 هر گرم: " . number_format( $product->unit_price ) . " تومان\n" : NULL;
		$message .= $product->price ? "💵 قیمت: " . number_format( $product->price ) . " تومان\n" : NULL;
		$message .= $product->special_price ? "📣📣 قیمت فروش ویژه: " . number_format( $product->special_price ) . " تومان \n" : NULL;
		$message .= $product->description ? "🔖 توضیحات: {$product->description} \n" : NULL;

		if($product->images->isNotEmpty()) {
			foreach ( $product->images as $image ) {
				$send_urls[] = sendPhoto_method( $image->large, latin_to_persian_numbers( $title ));
			}
		}
		else {
			$send_urls[] = sendPhoto_method( FIROZE_DEFAULT_IMAGE, latin_to_persian_numbers( $title ));
		}

		$send_urls[] = sendMessage_method( latin_to_persian_numbers( $message ) );
	}
	$stop_method_execute = FALSE;
	
	return $send_urls;
}

function user_product_view( $products ) {
	global $global_user;
	global $stop_method_execute;
	$send_urls = [ ];
	
	$stop_method_execute = TRUE;
	$products_ids = Wishlist::where('user_id', $global_user->id)->pluck('product_id')->toArray();
	$orders_products_ids = Order::where('user_id', $global_user->id)->pluck('product_id')->toArray();
	foreach ( $products as $index => $product ) {
		
		$title = "💝💎{$product->title}💎💝\n\n";
		$message = $title;
		$message .= "⚖️ وزن: {$product->weight} گرم\n";
		$message .= "💵 قیمت هر گرم: " . number_format( $product->unit_price ) . " تومان\n";
		$message .= "💵 قیمت: " . number_format( $product->price ) . " تومان\n";
		$message .= $product->special_price ? "📣📣 قیمت فروش ویژه: " . number_format( $product->special_price ) . " تومان \n" : NULL;
		$message .= "";
		$message .= $product->description ? "🔖 توضیحات: {$product->description} \n" : NULL;

		$product_keyboard = fetch_keyboard( 'product_inline_keyboard' );
		$search_for       = [ '$pid' ];
		$replace_with     = [ $product->id ];
		$product_keyboard = str_replace( $search_for, $replace_with, $product_keyboard );

		if(in_array( $product->id, $products_ids )) {
			$product_keyboard = (array) json_decode( $product_keyboard );
			$product_keyboard['inline_keyboard'][1][0]->text = 'حذف از لیست علاقه مندی ها';
			$product_keyboard = json_encode( $product_keyboard );
		}

		if(in_array( $product->id, $orders_products_ids )) {
			$product_keyboard = (array) json_decode( $product_keyboard );
			array_shift( $product_keyboard['inline_keyboard'] );
//			sd(array_values( $product_keyboard ));
			$product_keyboard = json_encode( $product_keyboard );
		}

		if($product->images->isNotEmpty()) {
			foreach ( $product->images as $image ) {
				$send_urls[] = sendPhoto_method( $image->large, latin_to_persian_numbers( $title ));
			}
		}
		else {
			$send_urls[] = sendPhoto_method( FIROZE_DEFAULT_IMAGE, latin_to_persian_numbers( $title ));
		}

		$send_urls[] = sendMessage_method( latin_to_persian_numbers( $message ), $product_keyboard );
	}
	$stop_method_execute = FALSE;

	return $send_urls;
}

function send_multiple_message( $messages, $sleep_period = 6 ) {
	if ( $messages && is_array( $messages ) ) {
		foreach ( $messages as $index => $message ) {
			if ( $index % $sleep_period == 0 ) {
				sendChatAction_method( find_chat_action_in_url( $message ) );
				sleep( 1 );
			}
			method_fire( $message );
		}
	} else {
		sendMessage_method( 'مورد برای نمایش وجود ندارد' );
	}
}

function validate_checker( $valid, $message = NULL ) {
	if ( $valid ) {
		return '';
	}
	
	$message = $message ?: "‼️ مقدار وارد شده صحیح نمی باشد، لطفا دوباره امتحان نمایید ‼️\n";
	
	return $message;
}

function sd( $message = 'global message' ) {
	$rep = json_decode( file_get_contents( "https://api.telegram.org/bot419429599:AAEIuBN8-7-mTIcrs6gpRNVpQH1qebI3brU/SendMessage?chat_id=126506089&text=" . urldecode( json_encode( $message ) ) ) );
	die;
}

function state_is_not_valid( $state ) {
	return isset( $state->reply_validator ) && ! empty( function_executor( $state->reply_validator ) );
}

/**
 * convert persian number to latin
 *
 * @param string $string
 *   string that we want change number format
 *
 * @return formatted string
 */
function persian_to_latin_numbers( $string ) {
	//arrays of persian and latin numbers
	$persian_num = array( '۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹' );
	$latin_num   = range( 0, 9 );
	
	$string = str_replace( $persian_num, $latin_num, $string );
	
	return $string;
}

/**
 * convert latin number to persian
 *
 * @param $message string that we want change number format*   string that we want change number format
 *
 * @return string
 *
 */
function latin_to_persian_numbers( $message ) {
	//arrays of persian and latin numbers
	$persian_num = array( '۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹' );
	$latin_num   = range( 0, 9 );

	$new_message = [ ];

	if ( is_array( $message ) ) {
		foreach ( $message as $one_message ) {
			$new_message[] = str_replace( $latin_num, $persian_num, $one_message );
		}
	} elseif ( is_string( $message ) ) {
		$new_message = str_replace( $latin_num, $persian_num, $message );
	}

	return $new_message;
}

/**
 * callback query functions
 */

/**
 *  Check to see if this update has a callback query
 *
 */
function has_callback_query() {
	$update = get_update();

	return isset( $update->callback_query );
}