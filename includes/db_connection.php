<?php
/*
   $dsn = 'mysql:host='.DB_SERVER.';dbname='.DB_NAME.';charset='.CHARSET.';';
   $db = new PDO($dsn,DB_USER, DB_PASS);
// */

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule;

$capsule->addConnection( [
	'driver'   => 'mysql',
	'host'     => DB_SERVER,
	'database' => DB_NAME,
	'username' => DB_USER,
	'password' => DB_PASS,
	'charset'  => 'utf8',
] );

$capsule->setAsGlobal();
$capsule->bootEloquent();