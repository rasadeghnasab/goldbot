<?php

if(has_callback_query()) {
	global $global_message;
	$params = isset( $global_message->params ) && is_object( $global_message->params ) ? (array) $global_message->params : [];
	if(!isset( $global_message->cb ) || empty( $global_message->cb )) {
		sd('your button has no callback function');
	}
	$reply = function_executor( $global_message->cb, $params);
	if(is_array( $reply->message )) {
		send_multiple_message( $reply->message );
	}
	else {
		$keyboard = isset( $reply->keyboard ) ? $reply->keyboard : NULL;
		sendMessage_method( latin_to_persian_numbers( $reply->message ) , $keyboard);
	}

	// answer callback
	$answer_callback = isset( $reply->answer_callback ) ? $reply->answer_callback : [];
	answerCallbackQuery_method($answer_callback);
	exit;
}