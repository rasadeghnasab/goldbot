<?php

// create global variables
$global_update         = get_update();
$global_message        = get_message();
$global_message_object = get_message_object();
$global_user           = get_user();
$global_user_view      = get_user_view();
$global_user_type      = get_user_type();
$global_user_state     = get_user_state();
$global_user_product   = get_user_product();
$user                  = user_find_or_create();
// variables
$stop_method_execute  = FALSE;
$skip_state_execution = 'مرحله بعد';
$back_a_state         = 'مرحله قبل';