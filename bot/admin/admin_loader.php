<?php
// load all admin files
require_once( SITE_ROOT . DS . 'bot/admin/admins_auth.php' );
require_once( SITE_ROOT . DS . 'bot/admin/keyboards.php' );
require_once( SITE_ROOT . DS . 'bot/admin/message_to_states.php' );
require_once( SITE_ROOT . DS . 'bot/admin/states.php' );
require_once( SITE_ROOT . DS . 'bot/admin/functions.php' );
require_once( SITE_ROOT . DS . 'bot/admin/callback_functions.php' );