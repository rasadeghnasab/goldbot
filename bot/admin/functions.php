<?php

/*****************************************
 **
 * Admin functions
 **
 *****************************************/
function say_hello() {
	return 'Hello to you';
}

function admin_change_view() {
	user_view_update( 'admin' );
}

function change_message() {
	return 'تغییر حالت به کاربر';
}

function success_or_failure_message(){
	
}

function main_message() {
	return 'از منو زیر یک گزینه را انتخاب نمایید';
}

function main_validator() {
	global $global_message;
	global $main_buttons;

	// $keyboard = fetch_keyboard_array('main');

	return validate_checker( in_array( $global_message, $main_buttons ), "دکمه کلیک شده اشتباه می باشد \n لطفا یکی از دکمه های زیر را انتخاب نمایید" );
}

function show_products() {
	$products = Product::with( 'images' )->get();

	return function_executor( get_user_type() . '_product_view', compact( 'products' ) );
}

function add_product_message() {
	return "در این قسمت میتوانید محصول خود را وارد نمایید \n ✨✨ توجه: هرکدام از اطلاعات را که مایل به وارد کردن آن نبودید میتوانید دکمه مرحله بعدی را فشار دهید ✨✨";
}

function add_product_validator() {
	global $global_message;

	// $keyboard = fetch_keyboard_array('start_adding_product');

	return validate_checker( $global_message == 'شروع افزودن محصول' );
}

function create_new_empty_product( $user = NULL ) {
	if ( ! $user ) {
		global $global_user;
		$user = $global_user;
	}
	$product = Product::create( [ 'user_id' => $user->id ] );

	return save_current_state( 'add product', $product->id );
}

function set_product_title_message() {
	return 'لطفا عنوان محصول خود را وارد نمایید ‼️این فیلد الزامی می باشد‼️';
}

/**
 * @return bool
 */
function set_product_title_validator() {
	global $global_message;

	// check message properties

	return validate_checker( strlen( $global_message ) <= 127 );
}

function save_product_title($title = null) {
	global $global_user;
	if(!$title) {
		global $global_message;
		$title = $global_message;
	}

	$user_state = UserState::where( 'user_id', $global_user->id )->first();

	return Product::updateOrCreate( [ 'id' => $user_state->product_id ], [ 'title' => $title ] );
}

function set_product_category_message() {
	return 'لطفا دسته بندی محصول خود را وارد نمایید ‼️این فیلد الزامی می باشد‼️';
}

/**
 * @return bool
 */
function set_product_category_validator() {
	global $global_message;
	global $categories_buttons;

	// check message properties

	return validate_checker( in_array( $global_message, $categories_buttons ) );
}

function save_product_category($category = null) {
	global $global_user;
	global $categories_buttons;

	if(!$category) {
		global $global_message;
		$category = $global_message;
	}

	$user_state = UserState::where( 'user_id', $global_user->id )->first();

	$category_id = array_search( $category, $categories_buttons ) ?: 0;

	// save product category field
	return Product::updateOrCreate( [ 'id' => $user_state->product_id ], [ 'category' => $category_id ] );
}

function set_product_weight_message() {
	return 'لطفا وزن محصول خود را وارد نمایید ‼️این فیلد الزامی می باشد‼️';
}

/**
 * @param null|double $weight
 *
 * @return bool
 */
function set_product_weight_validator($weight = null) {
	if(!$weight) {
		global $global_message;
		$weight = $global_message;		
	}
	
	return validate_checker( $weight > 0 && is_numeric( $weight ) && $weight < 200 );
}

function save_product_weight($weight = null) {
	global $global_user;
	if(!$weight) {
		global $global_message;
		$weight = $global_message;
	}
	
	$user_state = UserState::where( 'user_id', $global_user->id )->first();
	
	return Product::updateOrCreate( [ 'id' => $user_state->product_id ], [ 'weight' => $weight ] );
}

function set_product_image_message() {
	return 'لطفا تصویری از محصول خودر وارد نمایید ‼️این فیلد الزامی می باشد‼️';
}

function set_product_image_validator() {
	return validate_checker(check_user_send('photo') , 'لطفا برای این قسمت یک تصویر وارد نمایید!!');
}

function save_product_image() {
	global $global_update;
	global $global_user;

	$user_state = UserState::where( 'user_id', $global_user->id )->first();

	return product_image_insert( $global_update, $user_state->product_id );
}

function set_product_price_message() {
	return 'لطفا قیمت محصول خود را وارد نمایید ‼️این فیلد الزامی می باشد‼️';
}

/**
 * @param null|double $price
 *
 * @return bool
 */
function set_product_price_validator($price = null) {
	if(!$price) {
		global $global_message;
		$price = $global_message;
	}

	return validate_checker( $price > 0 && is_numeric( $price ) && $price > 10000 );
}

function save_product_price($price = null) {
	global $global_user;
	if(!$price) {
		global $global_message;
		$price = $global_message;
	}

	$user_state = UserState::where( 'user_id', $global_user->id )->first();
	
	return Product::updateOrCreate( [ 'id' => $user_state->product_id ], [ 'price' => $price ] );
}

function set_product_special_price_message() {
	return 'لطفا قیمت فروش ویژه محصول خود را وارد نمایید ✅این فیلد اختیاری است✅';
}

/**
 * @param null|double $special_price
 *
 * @return bool
 */
function set_product_special_price_validator($special_price = null) {
	global $global_user;

	if(!$special_price) {
		global $global_message;
		$special_price = $global_message;
	}

	$user_state = UserState::where( 'user_id', $global_user->id )->first();
	$product = Product::find($user_state->product_id);

	return validate_checker( $special_price > 0 && is_numeric( $special_price ) && $special_price > 10000 && $special_price < $product->price);
}

function save_product_special_price($special_price = null) {
	global $global_user;
	if(!$special_price) {
		global $global_message;
		$special_price = $global_message;
	}

	$user_state = UserState::where( 'user_id', $global_user->id )->first();
	
	return Product::updateOrCreate( [ 'id' => $user_state->product_id ], [ 'special_price' => $special_price ] );
}

function set_product_unit_price_message() {
	return 'لطفا قیمت هر گرم محصول خود را وارد نمایید ‼️این فیلد الزامی می باشد‼️';
}

/**
 * @param null|double $unit_price
 *
 * @return bool
 */
function set_product_unit_price_validator($unit_price = null) {
	if(!$unit_price) {
		global $global_message;
		$unit_price = $global_message;
	}
	
	return validate_checker( $unit_price > 0 && is_numeric( $unit_price ) && $unit_price > 10000 );
}

function save_product_unit_price($unit_price = null) {
	global $global_user;
	if(!$unit_price) {
		global $global_message;
		$unit_price = $global_message;
	}
	
	$user_state = UserState::where( 'user_id', $global_user->id )->first();
	
	return Product::updateOrCreate( [ 'id' => $user_state->product_id ], [ 'unit_price' => $unit_price ] );
}

function set_product_description_message() {
	return 'لطفا توضیحات محصول خود را وارد نمایید';
}

/**
 * @param null|double $description
 *
 * @return bool
 */
function set_product_description_validator($description = null) {
	if(!$description) {
		global $global_message;
		$description = $global_message;
	}
	
	return validate_checker( strlen( $description ) < 1400 );
}

function save_product_description($description = null) {
	global $global_user;
	if(!$description) {
		global $global_message;
		$description = $global_message;
	}
	
	$user_state = UserState::where( 'user_id', $global_user->id )->first();

	sendMessage_method( 'محصول شما با موفقیت ثبت شد' );
	
	return Product::updateOrCreate( [ 'id' => $user_state->product_id ], [ 'description' => $description ] );
}

function product_success_insert_message() {
	sendMessage_method( "محصول مورد نظر شما با موفقیت ثبت شد.\nاکنون شما به منو اصلی بازگشت داده میشوید" );
}

function show_orders_message() {
	// get all orders (connected to the users)
//	return orders();
	return 'order listing messages';
}

function show_orders_validator() {
	return validate_checker( TRUE );
}
