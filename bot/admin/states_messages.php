<?php
$states = [
	'main'               => 'منو اصلی',
	'add product'        => $main_buttons['add product'],
	'show products'      => $main_buttons['show products'],
	'show orders'        => $main_buttons['show orders'],
	'sales statistics'   => $main_buttons['sales statistics'],
	'members statistics' => $main_buttons['members statistics'],
	'user view'          => $main_buttons['user view'],
];
// TODO:: default must be determine by prev step.
$user_type     = 'admin';
$default_state = $user_type == 'admin' ? 'admin_main' : 'user_main';