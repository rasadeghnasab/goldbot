<?php

$main_buttons = [
	'add product'        => 'افزودن محصول',
	'show products'      => 'محصولات',
	'show orders'        => 'سفارش ها',
	'sales statistics'   => 'آمار فروش',
	'members statistics' => 'آمار کاربران',
	'user view'          => 'حالت کاربر',
];

$orders_buttons = [
	'today_orders'    => '🗓 سفارش های این هفته',
	'week_orders'     => '📆 سفارش های امروز',
	'canceled_orders' => '❌ سفارش های کنسل شده',
	'done_orders'     => '✅ سفارش های انجام شده',
];

$admin_keyboards = [
	'main' => [
		'keyboard'        => [
			[
				[ 'text' => $main_buttons['add product'] ],
				[ 'text' => $main_buttons['show products'] ],
				[ 'text' => $main_buttons['show orders'] ],
			],
			[
				[ 'text' => $main_buttons['sales statistics'] ],
				[ 'text' => $main_buttons['members statistics'] ],
			],
			/*[
				[ 'text' => $main_buttons['user view'] ],
			],*/
		],
		'resize_keyboard' => TRUE,
	],
	
	'categories' => [
		'keyboard'        => [
			[
				[ 'text' => $categories_buttons[0] ],
				[ 'text' => $categories_buttons[1] ],
				[ 'text' => $categories_buttons[2] ],
			],
			[
				[ 'text' => $categories_buttons[3] ],
				[ 'text' => $categories_buttons[4] ],
				[ 'text' => $categories_buttons[5] ],
			],
			[
				[ 'text' => $categories_buttons[6] ],
				[ 'text' => $categories_buttons[7] ],
				[ 'text' => $categories_buttons[8] ],
			],
		],
		'resize_keyboard' => TRUE,
	],

	'orders_categories' => [
		'keyboard'        => [
			[
				[ 'text' => $orders_buttons['today_orders'] ],
				[ 'text' => $orders_buttons['week_orders'] ],
			],
			[
				[ 'text' => $orders_buttons['canceled_orders'] ],
				[ 'text' => $orders_buttons['done_orders'] ],
			],
			[
				[ 'text' => 'منو اصلی' ],
			],
		],
		'resize_keyboard' => TRUE,
	],

	'next_state' => [
		'keyboard'        => [
			[
				[ 'text' => $skip_state_execution ],
			],
		],
		'resize_keyboard' => TRUE,
	],

	'main2' => [
		'keyboard'        => [
			[
				[ 'text' => $main_buttons['add product'] . ' 2' ],
				[ 'text' => $main_buttons['show products'] . ' 2' ],
				[ 'text' => $main_buttons['show orders'] . ' 2' ],
			],
			[
				[ 'text' => $main_buttons['sales statistics'] . ' 2' ],
				[ 'text' => $main_buttons['members statistics'] . ' 2' ],
			],
		],
		'resize_keyboard' => TRUE,
	],

	'hide' => [ 'remove_keyboard' => TRUE, ],

	'start_adding_product' => [
		'keyboard'          => [
			[
				[ 'text' => 'شروع افزودن محصول' ],
			],
		],
		"one_time_keyboard" => TRUE,
	],
];