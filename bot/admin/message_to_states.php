<?php

$message_to_state = [
	'main'               => 'منو اصلی',
	'add product'        => $main_buttons['add product'],
	'show products'      => $main_buttons['show products'],
	'show orders'        => $main_buttons['show orders'],
	'sales statistics'   => $main_buttons['sales statistics'],
	'members statistics' => $main_buttons['members statistics'],
	'change view'        => $main_buttons['user view'],
];