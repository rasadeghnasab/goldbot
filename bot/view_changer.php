<?php
// Get "view change command" and get a proper response
$view = isset( $main_buttons['admin view'] ) ? 'admin' : 'user';
if ($global_message == $main_buttons['admin view'] || $global_message == $main_buttons['user view']) {
	// Change user view in user table
	user_view_update($view);
	// go to main menu (in user or admin panel) and show proper message and keyboard
}