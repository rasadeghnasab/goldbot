<?php

if($global_message == '/start') {
	// check if user already exists in our system and send him/her proper message base on he is admin or user

	// check if this is first time user enters our system (admin / user)
	if($global_user_type == 'admin') {
		sendMessage_method('سلام ادمین', fetch_keyboard());
	}
	else {
		sendMessage_method('💍 به جواهر فروشی فیروز خوش آمدید 💍', fetch_keyboard());
	}

	$result = save_current_state( 'main' );

	exit;
}