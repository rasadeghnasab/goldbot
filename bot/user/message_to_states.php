<?php

$message_to_state = [
	'main'          => 'منو اصلی',
	'categories'    => $main_buttons['categories'],
	'special sells' => $main_buttons['special sells'],
	'daily price'   => $main_buttons['daily price'],
	'my favorites'  => $main_buttons['my favorites'],
	'my account'    => $main_buttons['my account'],
	'admin view'    => $main_buttons['admin view'],
];