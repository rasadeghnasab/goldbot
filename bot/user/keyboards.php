<?php

$main_buttons = [
	'categories'    => 'دسته بندی ها',
	'special sells' => 'محصولات فروش ویژه',
	'daily price'   => 'قیمت روز',
	'my favorites'  => 'علاقه مندی های من',
	'my account'    => 'سفارشات من',
//	'admin view'    => 'صفحه ادمین',
];

$user_keyboards = [
	'main' => [
		'keyboard'        => [
			[
				[ 'text' => $main_buttons['categories'] ],
				[ 'text' => $main_buttons['special sells'] ],
			],
			[
				[ 'text' => $main_buttons['daily price'] ],
			],
			[
				[ 'text' => $main_buttons['my favorites'] ],
				[ 'text' => $main_buttons['my account'] ],
			],
		],
		'resize_keyboard' => TRUE,
	],

	'categories' => [
		'keyboard'        => [
			[
				[ 'text' => $categories_buttons[0] ],
				[ 'text' => $categories_buttons[1] ],
				[ 'text' => $categories_buttons[2] ],
			],
			[
				[ 'text' => $categories_buttons[3] ],
				[ 'text' => $categories_buttons[4] ],
				[ 'text' => $categories_buttons[5] ],
			],
			[
				[ 'text' => $categories_buttons[6] ],
				[ 'text' => $categories_buttons[7] ],
				[ 'text' => $categories_buttons[8] ],
			],
			[
				[ 'text' => 'منو اصلی ⬅' ],
			],
		],
		'resize_keyboard' => TRUE,
	],

//	/*
	'product_inline_keyboard' => [
		'inline_keyboard'=>[
//			[['text'=>"خرید", 'url'=>'http://example.com/this-is-a-url-to-order-complete-page']],
		    [
				['text'=>"ثبت سفارش",'callback_data'=>'{"cb":"userOrderSubmit","params":{"pid":"$pid"}}']
			],
		    [
				['text'=>"افزودن به علاقه مندی ها",'callback_data'=>'{"cb":"userToggleWishList","params":{"pid":"$pid"}}']
		    ]
		]
	],
	//*/

	'submit' => [
		'keyboard'        => [
			[
				[ 'text' => 'ثبت' ],
			],
		],
		'resize_keyboard' => TRUE,
	],

	'hide' => [ 'hide_keyboard' => TRUE, ],
];

if ( get_user_type( $global_user, FALSE ) == 'admin' ) {
	//	$user_keyboards['main']['keyboard'][] = [ 'text' => $main_buttons['admin view'] ];
}