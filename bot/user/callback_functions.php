<?php

function userToggleWishList( $pid, $user_id = NULL ) {
	$reply = [ ];

	if ( ! $user_id ) {
		global $global_user;
		$user_id = $global_user->id;
	}

	$result                                 = toggleWishlist( $user_id, $pid );
	$reply['message']                       = '';
	$reply['answer_callback']['text']       = $result['message'];
	$reply['answer_callback']['show_alert'] = TRUE;

	//	$reply['keyboard'] =

	return (object) $reply;
}

function userOrderSubmit( $pid, $user_id = NULL ) {
	$reply = [ ];
	$alert_message = 'برای ثبت سفارش می بایست مراحل زیر را تکمیل نمایید';

	if ( ! $user_id ) {
		global $global_user;
		$user_id = $global_user->id;
	}

	// If user already submitted an order for on this product we skip this step
	$order = Order::where(['user_id' => $user_id, 'product_id' => $pid])->first();

	if(!$order) {
		// submit user order
		$order = Order::create(['user_id' => $user_id, 'product_id' => $pid]);
		$new_state_name  = 'get user real fn ln';

		// send user to contact info states
		$processed_state = next_state_handler( $new_state_name );
		save_current_state( $new_state_name );
		$reply['keyboard'] = $processed_state->keyboard;
		$reply['message']  = $processed_state->message;
	}
	else {
		// if user already entered his contact information and it's less that 30 days, we skip this step
		$alert_message = 'شما قبلا برای این محصول سفارش ثبت کرده اید';
	}

	$reply['answer_callback']['text']       = $alert_message;
	$reply['answer_callback']['show_alert'] = TRUE;

	return (object) $reply;
}