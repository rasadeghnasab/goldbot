<?php

/*****************************************
 **
 * User functions
 **
 *****************************************/
function user_main_message() {
	return 'از منو اصلی یک گزینه را انتخاب نمایید';
}

function user_main_validator() {
	return validate_checker( TRUE );
}

function user_change_message() {
	return 'تغییر حالت';
}

function user_change_view() {
	user_view_update( 'admin' );
}

function user_categories_message() {
	return 'از دسته های زیر یکی را انتخاب نمایید';
}

function user_categories_validator() {
	global $global_message;
	global $categories_buttons;

	return validate_checker( array_search( $global_message, $categories_buttons ) );
}

function show_special_sells_message() {
	$products = Product::whereNotNull( 'special_price' )->get();

	return function_executor( get_user_type() . '_product_view', compact( 'products' ) );
}

function show_special_sells_validator() {
	return validate_checker( TRUE );
}

function show_daily_price_message() {
	$message = '💎قيمت طلاوسكه💎

امـامـی          ۱/۲۱۸/۰۰۰💵

نیـم          ۶۳۲/۰۰۰ 💵

ربـع          ۳۷۳/۰۰۰ 💵

گرمـی         ۲۴۸/۰۰۰ 💵
 
خام طلا           ١١٧/٠٠٠💵

دلار                 ٣/٨٢٠💵';

	return $message;
}

function show_daily_price_validator() {
	return validate_checker( TRUE );
}

function show_user_favorites_message() {
	global $global_message;
	global $global_user;

	//	$category_id = message_to_category($global_message);

	//	return 'list all products on category ' . $global_message;
	$products_ids = Wishlist::where( 'user_id', $global_user->id )
	                        ->pluck( 'product_id' )
	                        ->toArray()
	;
	$products     = Product::whereIn( 'id', $products_ids )->get();

	return $products->isNotEmpty() ? function_executor( get_user_type() . '_product_view', compact( 'products' ) ) : 'در لیست علاقه مندی های شما موردی وجود ندارد';
}

function show_user_favorites_validator() {
	return validate_checker( TRUE );
}

function show_my_account_message() {
	global $global_message;
	global $global_user;

	//	$category_id = message_to_category($global_message);

	//	return 'list all products on category ' . $global_message;
	$products_ids = Order::where( 'user_id', $global_user->id )
	                        ->pluck( 'product_id' )
	                        ->toArray()
	;
	$products     = Product::whereIn( 'id', $products_ids )->get();

	return $products->isNotEmpty() ? function_executor( get_user_type() . '_product_view', compact( 'products' ) ) : 'شما تاکنون سفارشی ثبت نکرده اید';
}

function show_my_account_validator() {
	return validate_checker( TRUE );
}

function show_products_on_category_message() {
	global $global_message;
	
	$category_id = message_to_category( $global_message );

	//	return 'list all products on category ' . $global_message;
	$products = Product::where( 'category', $category_id )->get();

	return $products->isNotEmpty() ? function_executor( get_user_type() . '_product_view', compact( 'products' ) ) : 'هیچ محصولی در این کتگوری یافت نشد';
}

function show_products_on_category_validator() {
	global $global_message;
	//	global $categories_buttons;
	global $main_buttons;

	//	return validate_checker( in_array( $global_message, $categories_buttons ) );
	return validate_checker( TRUE );
}

function get_user_fn_ln_message() {
	return 'لطفا نام و نام خانوادگی خود را وارد نمایید';
}

function get_user_fn_ln_validate() {
	global $global_message;

	return validate_checker( ! empty( $global_message ) && mb_strlen( $global_message ) >= 6 );
}

function save_user_fn_ln( $fn_ln = NULL ) {
	global $global_user;

	if ( ! $fn_ln ) {
		global $global_message;
		$fn_ln = $global_message;
	}

	$user = User::where( 'id', $global_user->id )
	            ->update( [ 'real_fn_ln' => $fn_ln ] )
	;;

	return $user;
}

function get_user_phone_message() {
	return 'لطفا شماره موبایل خود را وارد نمایید';
}

function get_user_phone_validate() {
	global $global_message;
	
	return validate_checker( is_numeric( $global_message ) && preg_match( '/(09)\d{9}/', $global_message ), "‼ شماره موبایل وارد شده صحیح نمی باشد ‼\n" );
}

function save_user_mobile_number( $mobile = NULL ) {
	global $global_user;
	
	if ( ! $mobile ) {
		global $global_message;
		$mobile = $global_message;
	}
	
	$user = User::where( 'id', $global_user->id )
	            ->update( [ 'mobile' => $mobile ] )
	;;
	
	return $user;
}

