<?php
// load all user files
require_once( SITE_ROOT . DS . 'bot/user/keyboards.php' );
require_once( SITE_ROOT . DS . 'bot/user/message_to_states.php' );
require_once( SITE_ROOT . DS . 'bot/user/states.php' );
require_once( SITE_ROOT . DS . 'bot/user/functions.php' );
require_once( SITE_ROOT . DS . 'bot/user/callback_functions.php' );