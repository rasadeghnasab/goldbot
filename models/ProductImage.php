<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class ProductImage extends Eloquent {
	
	protected $fillable = [
		'product_id',
		'small',
	    'medium',
	    'large'
	];
	
	public $timestamps = false;
	
	public function product() {
		return $this->belongsTo( 'Product' );
	}
}