<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Order extends Eloquent {
	protected $fillable = [
		'user_id',
		'product_id',
		'authority',
		'ref_id',
		'order_status',
	];
	
	public function user() {
		return $this->belongsTo( 'User' );
	}

	public function product() {
		return $this->belongsTo( 'Product' );
	}
	
}