<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Wishlist extends Eloquent {
	protected $fillable = [
		'user_id',
		'product_id',
	];
	
	public $timestamps = false;

	public function user() {
		return $this->belongsTo( 'User' );
	}

	public function product() {
		return $this->belongsTo( 'Product' );
	}
	
}