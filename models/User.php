<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class User extends Eloquent {
	

	protected $fillable = [
		'id',
		'username',
		'first_name',
		'last_name',
		'real_fn_ln',
		'mobile',
	];

	public function wishlists(  ) {
		return $this->hasMany( 'Wishlist' );
	}
}