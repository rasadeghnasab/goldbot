<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Product extends Eloquent {
	protected $fillable = [
		'user_id',
		'category',
		'title',
		'weight',
		'price',
		'unit_price',
		'special_price',
		'description',
	];

	//	public $timestamps = false;

	public function user() {
		return $this->belongsTo( 'User' );
	}

	public function images() {
		return $this->hasMany( 'ProductImage' );
	}
}