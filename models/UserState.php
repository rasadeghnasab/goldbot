<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class UserState extends Eloquent {
	protected $table = 'users_states';
	// TODO:: this might be change in future
	protected $primaryKey = 'user_id';
	public $timestamps =  false;

	protected $fillable = [
		'user_id',
//	    'state_id',
	    'state',
	    'product_id',
	];

	public function user() {
		return $this->belongsTo( 'User' );
	}

	public function product() {
		return $this->hasOne( 'Product' );
	}
}